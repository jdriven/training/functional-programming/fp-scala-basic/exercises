package fpinscala.chapter11

import fpinscala.chapter11.Monad._
import fpinscala.generators.FPGeneratorConfiguration
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import org.scalatestplus.scalacheck.ScalaCheckDrivenPropertyChecks

class MonadSpecId
  extends AnyWordSpecLike
    with Matchers
    with ScalaCheckDrivenPropertyChecks
    with FPGeneratorConfiguration {

  "A Monad[Id]" when {
    "unit" should {
      "wrap a value" in {
        idMonad.unit(1).value should be(1)
      }
    }

    "flatMap" should {
      "substitute variables" in {
        val id = for {
          a <- Id("Hello, ")
          b <- Id("monad!")
        } yield a + b

        id.value should be("Hello, monad!")
      }
    }
  }
}
