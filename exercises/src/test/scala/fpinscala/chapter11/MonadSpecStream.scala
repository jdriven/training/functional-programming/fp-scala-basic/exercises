package fpinscala.chapter11

import fpinscala.chapter11.Monad._
import fpinscala.generators.FPGeneratorConfiguration
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import org.scalatestplus.scalacheck.ScalaCheckDrivenPropertyChecks

class MonadSpecStream
  extends AnyWordSpecLike
    with Matchers
    with ScalaCheckDrivenPropertyChecks
    with FPGeneratorConfiguration {

  "A Monad[Stream]" when {

    "map" should {
      "Functor laws: identity" in {
        forAll("o") { t1: LazyList[Unit] =>
          streamMonad.map(t1)(identity) should be(t1)
        }
      }
      "Functor laws: associativity" in {
        forAll("f1", "f2", "o") { (f1: Int => Int, f2: Int => Int, o: LazyList[Int]) =>
          streamMonad.map(o)(f1).map(f2) should be(streamMonad.map(o)(f2.compose(f1)))
        }
      }
    }

    "flatmap" should {
      "Monad laws: Left identity: LazyList(x) flatMap f == f(x)" in {
        forAll("x", "f") { (x: Int, f: Int => LazyList[Int]) =>
          streamMonad.flatMap(LazyList(x))(f) should be(f(x))
        }
      }
      "Monad laws: Right identity: (m flatMap LazyList(_) === m" in {
        forAll("m") { m: LazyList[Int] =>
          streamMonad.flatMap(m)(LazyList(_)) should be(m)
        }
      }
      "Monad laws: Associativity: (m flatMap f) flatMap g === m flatMap { x => f(x) flatMap g }" in {
        forAll("m", "f", "g") { (m: LazyList[Int], f: Int => LazyList[Int], g: Int => LazyList[Int]) =>
          streamMonad.flatMap(streamMonad.flatMap(m)(f))(g) should be(streamMonad.flatMap(m) { x =>
            streamMonad.flatMap(f(x))(g)
          })
        }
      }
    }

    "map2" should {
      def add(a: Int, b: Int): Int = a + b

      "first is empty" in {
        streamMonad.map2(LazyList(), LazyList(2))(add) should be(LazyList())
      }
      "second is empty" in {
        streamMonad.map2(LazyList(2), LazyList())(add) should be(LazyList())
      }
      "both are LazyList" in {
        streamMonad.map2(LazyList(2), LazyList(3))(add) should be(LazyList(5))
      }
    }

    "sequence" should {
      "empty LazyList" in {
        streamMonad.sequence(List()) should be(LazyList(List()))
      }
      "only one empty" in {
        streamMonad.sequence(List(LazyList())) should be(LazyList())
      }
      "LazyList with one empty" in {
        streamMonad.sequence(List(LazyList(2), LazyList(3), LazyList(4), LazyList())) should be(LazyList())
      }
      "LazyList with list empty" in {
        streamMonad.sequence(List(LazyList(2), LazyList(3), LazyList(4))) should be(LazyList(List(2, 3, 4)))
      }
    }

    "traverse" should {
      def add1(a: LazyList[Int]): LazyList[Int] = a.map(a => a + 1)

      "empty LazyList" in {
        streamMonad.traverse(List())(add1) should be(LazyList(List()))
      }
      "only one empty" in {
        streamMonad.traverse(List(LazyList()))(add1) should be(LazyList())
      }
      "LazyList with one empty" in {
        streamMonad.traverse(List(LazyList(2), LazyList(3), LazyList(4), LazyList()))(add1) should be(LazyList())
      }
      "LazyList with none empty" in {
        streamMonad.traverse(List(LazyList(2), LazyList(3), LazyList(4)))(add1) should be(LazyList(List(3, 4, 5)))
      }
    }
  }
}
