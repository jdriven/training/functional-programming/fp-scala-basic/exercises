package fpinscala.chapter11

import fpinscala.chapter11.Monad._
import fpinscala.generators.FPGeneratorConfiguration
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import org.scalatestplus.scalacheck.ScalaCheckDrivenPropertyChecks

class MonadSpecReader
    extends AnyWordSpecLike
    with Matchers
    with ScalaCheckDrivenPropertyChecks
    with FPGeneratorConfiguration {

  "A Monad[Reader]" when {
    "unit" should {
      "wraps a value without a dependency" in {
        readerMonad[Any].unit(1).run(()) should be(1)
      }
    }

    "flatMap" should {
      "use the same dependency twice for both Readers" in {
        val needsName = for {
          helloName <- Reader((s: String) => s"Hello $s")
          yourNameIs <- Reader((s: String) => s"Your name is $s")
        } yield s"$helloName. $yourNameIs."

        needsName.run("Chiel") should be("Hello Chiel. Your name is Chiel.")
      }
    }
  }
}
