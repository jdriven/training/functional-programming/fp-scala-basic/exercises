package fpinscala.chapter11

import fpinscala.chapter11.Monad._
import fpinscala.generators.FPGeneratorConfiguration
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import org.scalatestplus.scalacheck.ScalaCheckDrivenPropertyChecks

class MonadSpecList
  extends AnyWordSpecLike
    with Matchers
    with ScalaCheckDrivenPropertyChecks
    with FPGeneratorConfiguration {

  "A Monad[List]" when {

    "map" should {
      "Functor laws: identity" in {
        forAll("o") { t1: List[Unit] =>
          listMonad.map(t1)(identity) should be(t1)
        }
      }
      "Functor laws: associativity" in {
        forAll("f1", "f2", "o") { (f1: Int => Int, f2: Int => Int, o: List[Int]) =>
          listMonad.map(o)(f1).map(f2) should be(listMonad.map(o)(f2.compose(f1)))
        }
      }
    }

    "flatmap" should {
      "Monad laws: Left identity: List(x) flatMap f == f(x)" in {
        forAll("x", "f") { (x: Int, f: Int => List[Int]) =>
          listMonad.flatMap(List(x))(f) should be(f(x))
        }
      }
      "Monad laws: Right identity: (m flatMap List(_) === m" in {
        forAll("m") { m: List[Int] =>
          listMonad.flatMap(m)(List(_)) should be(m)
        }
      }
      "Monad laws: Associativity: (m flatMap f) flatMap g === m flatMap { x => f(x) flatMap g }" in {
        forAll("m", "f", "g") { (m: List[Int], f: Int => List[Int], g: Int => List[Int]) =>
          listMonad.flatMap(listMonad.flatMap(m)(f))(g) should be(listMonad.flatMap(m) { x =>
            listMonad.flatMap(f(x))(g)
          })
        }
      }
    }

    "map2" should {
      def add(a: Int, b: Int): Int = a + b

      "first is empty" in {
        listMonad.map2(List(), List(2))(add) should be(List())
      }
      "second is empty" in {
        listMonad.map2(List(2), List())(add) should be(List())
      }
      "both are List" in {
        listMonad.map2(List(2), List(3))(add) should be(List(5))
      }
    }

    "sequence" should {
      "empty list" in {
        listMonad.sequence(List()) should be(List(List()))
      }
      "only one empty" in {
        listMonad.sequence(List(List())) should be(List())
      }
      "list with one empty" in {
        listMonad.sequence(List(List(2), List(3), List(4), List())) should be(List())
      }
      "list with List() empty" in {
        listMonad.sequence(List(List(2), List(3), List(4))) should be(List(List(2, 3, 4)))
      }
    }

    "traverse" should {
      def add1(a: List[Int]): List[Int] = a.map(a => a + 1)

      "empty list" in {
        listMonad.traverse(List())(add1) should be(List(List()))
      }
      "only one empty" in {
        listMonad.traverse(List(List()))(add1) should be(List())
      }
      "list with one empty" in {
        listMonad.traverse(List(List(2), List(3), List(4), List()))(add1) should be(List())
      }
      "list with List() empty" in {
        listMonad.traverse(List(List(2), List(3), List(4)))(add1) should be(List(List(3, 4, 5)))
      }
    }
  }
}
