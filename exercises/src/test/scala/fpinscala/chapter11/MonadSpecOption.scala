package fpinscala.chapter11

import fpinscala.generators.FPGeneratorConfiguration
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import org.scalatestplus.scalacheck.ScalaCheckDrivenPropertyChecks
import fpinscala.chapter11.Monad._

import scala.util.Try

class MonadSpecOption
  extends AnyWordSpecLike
    with Matchers
    with ScalaCheckDrivenPropertyChecks
    with FPGeneratorConfiguration {

  "A Monad[Option]" when {

    "map" should {
      "Functor laws: identity" in {
        forAll("o") { t1: Option[Unit] =>
          optionMonad.map(t1)(identity) should be(t1)
        }
      }
      "Functor laws: associativity" in {
        forAll("f1", "f2", "o") { (f1: Int => Int, f2: Int => Int, o: Option[Int]) =>
          optionMonad.map(o)(f1).map(f2) should be(
            optionMonad.map(o)(f2.compose(f1)))
        }
      }
    }

    "flatmap" should {
      "Monad laws: Left identity: Some(x) flatMap f == f(x)" in {
        forAll("x", "f") { (x: Int, f: Int => Option[Int]) =>
          optionMonad.flatMap(Some(x))(f) should be(f(x))
        }
      }
      "Monad laws: Right identity: (m flatMap Some(_) === m" in {
        forAll("m") { m: Option[Int] =>
          optionMonad.flatMap(m)(Some(_)) should be(m)
        }
      }
      "Monad laws: Associativity: (m flatMap f) flatMap g === m flatMap { x => f(x) flatMap g }" in {
        forAll("m", "f", "g") { (m: Option[Int], f: Int => Option[Int], g: Int => Option[Int]) =>
          optionMonad.flatMap(optionMonad.flatMap(m)(f))(g) should be(
            optionMonad.flatMap(m) { x =>
              optionMonad.flatMap(f(x))(g)
            })
        }
      }
    }

    "map2" should {
      def add(a: Int, b: Int): Int = a + b

      "first is none" in {
        optionMonad.map2(None, Some(2))(add) should be(None)
      }
      "second is none" in {
        optionMonad.map2(Some(2), None)(add) should be(None)
      }
      "both are some" in {
        optionMonad.map2(Some(2), Some(3))(add) should be(Some(5))
      }
    }

    "sequence" should {
      "empty list" in {
        optionMonad.sequence(List()) should be(Some(List()))
      }
      "only one none" in {
        optionMonad.sequence(List(None)) should be(None)
      }
      "list with one none" in {
        optionMonad.sequence(List(Some(2), Some(3), Some(4), None)) should be(
          None)
      }
      "list with none none" in {
        optionMonad.sequence(List(Some(2), Some(3), Some(4))) should be(
          Some(List(2, 3, 4)))
      }
    }

    "traverse" should {
      def add1(a: Option[Int]): Option[Int] = a.map(a => a + 1)

      "empty list" in {
        optionMonad.traverse(List())(add1) should be(Some(List()))
      }
      "only one none" in {
        optionMonad.traverse(List(None))(add1) should be(None)
      }
      "list with one none" in {
        optionMonad.traverse(List(Some(2), Some(3), Some(4), None))(add1) should be(
          None)
      }
      "list with none none" in {
        optionMonad.traverse(List(Some(2), Some(3), Some(4)))(add1) should be(
          Some(List(3, 4, 5)))
      }
    }

    "replicateM" should {
      "replicate Some" in {
        forAll("i", "a") { (n: List[Int], a: String) =>
          optionMonad.replicateM(n.size, Some(a)) should be(
            Some(List.fill(n.size)(a)))
        }
      }
      "replicate None" in {
        forAll("i") { n: List[Int] =>
          val replicated = optionMonad.replicateM(n.size, None)
          if (n.isEmpty)
            replicated should be(Some(List()))
          else
            replicated should be(None)
        }
      }
    }

    "filterM" should {
      "keep on Some" in {
        forAll("list") { (list: List[Int]) =>
          val filteredM = optionMonad.filterM(list)(a =>
            Some(a).map(_ % 2 == 0).filter(identity))
          if (list.filter(_ % 2 == 0) == list)
            filteredM should be(Some(list))
          else filteredM should be(None)
        }
      }

      "remove on None" in {
        forAll("list") { n: List[Int] =>
          val filteredM = optionMonad.filterM(n)(_ => None)
          if (n.isEmpty)
            filteredM should be(Some(List()))
          else
            filteredM should be(None)
        }
      }
    }

    "compose" should {
      "combine a => F[B] and b => F[C]" in {
        val f = optionMonad.compose[List[String], String, Int](_.headOption, s => Try(Integer.parseInt(s)).toOption)
        f(List()) should be(None)
        f(List("1", "2", "3")) should be(Some(1))
      }

      "Monad laws (compose): Associativity: (m flatMap f) flatMap g === m flatMap { x => f(x) flatMap g }" in {
        forAll("m", "f", "g") { (m: Option[Int], f: Int => Option[Int], g: Int => Option[Int]) =>
          optionMonad._flatMap(optionMonad._flatMap(m)(f))(g) should be(
            optionMonad._flatMap(m) { x =>
              optionMonad._flatMap(f(x))(g)
            })
        }
      }
    }

    "join" should {
      "flatten a F[F[A]] to F[A]" in {
        optionMonad.join(Some(Some(1))) should be(Some(1))
        optionMonad.join(Some(None)) should be(None)
        optionMonad.join(None) should be(None)
      }

      "combine(join)  a => F[B] and b => F[C]" in {
        val f = optionMonad.__compose[List[String], String, Int](_.headOption, s => Try(Integer.parseInt(s)).toOption)
        f(List()) should be(None)
        f(List("1", "2", "3")) should be(Some(1))
      }

      "Monad laws (join): Associativity: (m flatMap f) flatMap g === m flatMap { x => f(x) flatMap g }" in {
        forAll("m", "f", "g") { (m: Option[Int], f: Int => Option[Int], g: Int => Option[Int]) =>
          optionMonad.__flatMap(optionMonad.__flatMap(m)(f))(g) should be(
            optionMonad.__flatMap(m) { x =>
              optionMonad.__flatMap(f(x))(g)
            })
        }
      }
    }
  }
}
