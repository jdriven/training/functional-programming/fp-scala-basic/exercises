package fpinscala.chapter10

import fpinscala.chapter10.Monoid._
import fpinscala.generators.FPGeneratorConfiguration
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import org.scalatestplus.scalacheck.ScalaCheckDrivenPropertyChecks

class MonoidSpec
  extends AnyWordSpecLike
    with Matchers
    with ScalaCheckDrivenPropertyChecks
    with FPGeneratorConfiguration {

  "intaddition monoid" when {
    "using the op" should {
      "satisfy the identity law" in {
        intAddition.op(intAddition.zero, 24862) should be(24862)
        intAddition.op(98746, intAddition.zero) should be(98746)
      }
      "satisfy the associativity law" in {
        List(1, 5, 8974, 33).foldRight(intAddition.zero)(intAddition.op) should be(9013)
        List(1, 5, 8974, 33).foldLeft(intAddition.zero)(intAddition.op) should be(9013)
      }
    }
  }
  "intMultiplication monoid" when {
    "using the op" should {
      "satisfy the identity law" in {
        intMultiplication.op(intMultiplication.zero, 24862) should be(24862)
        intMultiplication.op(98746, intMultiplication.zero) should be(98746)
      }
      "satisfy the associativity law" in {
        List(1, 5, 8974, 33).foldRight(intMultiplication.zero)(intMultiplication.op) should be(1480710)
        List(1, 5, 8974, 33).foldLeft(intMultiplication.zero)(intMultiplication.op) should be(1480710)
      }
    }
  }
  "booleanOr monoid" when {
    "using the op" should {
      "satisfy the identity law" in {
        booleanOr.op(booleanOr.zero, true) should be(true)
        booleanOr.op(true, booleanOr.zero) should be(true)
        booleanOr.op(booleanOr.zero, false) should be(false)
        booleanOr.op(false, booleanOr.zero) should be(false)
      }
      "satisfy the associativity law" in {
        List(false, false, false).foldRight(booleanOr.zero)(booleanOr.op) should be(false)
        List(false, false, false).foldLeft(booleanOr.zero)(booleanOr.op) should be(false)

        List(true, true, true).foldRight(booleanOr.zero)(booleanOr.op) should be(true)
        List(true, true, true).foldLeft(booleanOr.zero)(booleanOr.op) should be(true)

        List(false, false, true).foldRight(booleanOr.zero)(booleanOr.op) should be(true)
        List(false, false, true).foldLeft(booleanOr.zero)(booleanOr.op) should be(true)
      }
    }
  }
  "booleanAnd monoid" when {
    "using the op" should {
      "satisfy the identity law" in {
        booleanAnd.op(booleanAnd.zero, true) should be(true)
        booleanAnd.op(true, booleanAnd.zero) should be(true)
        booleanAnd.op(booleanAnd.zero, false) should be(false)
        booleanAnd.op(false, booleanAnd.zero) should be(false)
      }
      "satisfy the associativity law" in {
        List(false, false, false).foldRight(booleanAnd.zero)(booleanAnd.op) should be(false)
        List(false, false, false).foldLeft(booleanAnd.zero)(booleanAnd.op) should be(false)

        List(true, true, true).foldRight(booleanAnd.zero)(booleanAnd.op) should be(true)
        List(true, true, true).foldLeft(booleanAnd.zero)(booleanAnd.op) should be(true)

        List(false, false, true).foldRight(booleanAnd.zero)(booleanAnd.op) should be(false)
        List(false, false, true).foldLeft(booleanAnd.zero)(booleanAnd.op) should be(false)
      }
    }
  }
  "option monoid" when {
    "using the op" should {
      "satisfy the identity law" in {
        optionMonoid.op(optionMonoid.zero, Some(4)) should be(Some(4))
        optionMonoid.op(Some(4), optionMonoid.zero) should be(Some(4))
      }
      "satisfy the associativity law" in {
        List(None, Some(3), None).foldRight[Option[Int]](optionMonoid.zero)(optionMonoid.op) should be(Some(3))
        List(None, Some(3), None).foldLeft[Option[Int]](optionMonoid.zero)(optionMonoid.op) should be(Some(3))
        List(None, None, None).foldLeft[Option[Int]](optionMonoid.zero)(optionMonoid.op) should be(None)
      }
    }
  }
  "endo monoid" when {
    "using the op" should {
      val plusOne = (a: Int) => a + 1
      val plusTwo = (a: Int) => a + 2
      val plusThree = (a: Int) => a + 1
      "satisfy the identity law" in {
        endoMonoid.op(endoMonoid.zero, plusOne)(2) should be(plusOne(2))
        endoMonoid.op(plusOne, endoMonoid.zero)(2) should be(plusOne(2))
      }
      "satisfy the associativity law" in {
        List(plusOne, plusTwo, plusThree).foldRight[Int => Int](endoMonoid.zero)(endoMonoid.op)(1) should
          be(List(plusOne, plusTwo, plusThree).foldLeft[Int => Int](endoMonoid.zero)(endoMonoid.op)(1))
      }
    }
  }
  "foldMap" when {
    "with StringMonoid" in {
      foldMap(List(1,2,3), stringMonoid)(a => a.toString) should be("123")
    }
    "with intMonoid" in {
      foldMap(List("aa", "a", "aaaa"), intAddition)(a => a.length) should be(7)
      foldMap(List("aa", "a", "aaaa"), intMultiplication)(a => a.length) should be(8)
    }
  }
  "foldRight with foldMap" when {
    "given a list, should return in same order" in {
      foldRight(List(1, 2, 3))(List[Int]())((h, acc) => h :: acc) should be(List(1, 2, 3))
    }
    "should return the same as a list's foldRight" in {
      List(1, 5, 8974, 33).foldRight(intAddition.zero)(intAddition.op) should be(foldRight[Int, Int](List(1, 5, 8974, 33))(intAddition.zero)(intAddition.op))
    }
  }
  "foldLeft with foldMap" when {
    "given a list, should return in reverse order" in {
      foldLeft(List(1, 2, 3))(List[Int]())((acc, h) => h :: acc) should be(List(3, 2, 1))
    }
    "should return the same as a list's foldRight" in {
      List(1, 5, 8974, 33).foldLeft(intAddition.zero)(intAddition.op) should be(foldLeft[Int, Int](List(1, 5, 8974, 33))(intAddition.zero)(intAddition.op))
    }
  }
  "wc monoid" when {
    "using the op" should {
      "satisfy the identity law" in {
        wcMonoid.op(wcMonoid.zero, Stub("s")) should be(Stub("s"))
        wcMonoid.op(Stub("s"), wcMonoid.zero) should be(Stub("s"))
        wcMonoid.op(Part("s", 1, "v"), wcMonoid.zero) should be(Part("s", 1, "v"))
        wcMonoid.op(wcMonoid.zero, Part("s", 1, "v")) should be(Part("s", 1, "v"))
      }
      "satisfy the associativity law" in {
        List(Stub("a"), Stub("a"), Part("s", 1, "v"), Stub("b"),Stub("b"), Stub("b")).foldRight(wcMonoid.zero)(wcMonoid.op) should be(Part("aas", 1, "vbbb"))
        List(Stub("a"), Stub("a"), Part("s", 1, "v"), Stub("b"),Stub("b"), Stub("b")).foldLeft(wcMonoid.zero)(wcMonoid.op) should be(Part("aas", 1, "vbbb"))
      }
    }
    "using count" should {
      "count the words" in {
        count("bla bla bla bla bla") should be(5)
        count("bla") should be(1)
        count("bla   ") should be(1)
        count("   bla   ") should be(1)
        count("   bla") should be(1)
        count("   ") should be(0)
        count(" ") should be(0)
        count("") should be(0)
      }
    }
  }
  "listFoldable" when {
    "using foldRight with a list" should {
      "return the same list" in {
        ListFoldable.foldRight(List(1, 2, 3))(List[Int]())((h, acc) => h :: acc) should be(List(1, 2, 3))
      }
    }
    "using foldLeft with a list" should {
      "return the same list in reverse order" in {
        ListFoldable.foldLeft(List(1, 2, 3))(List[Int]())((acc, h) => h :: acc) should be(List(3, 2, 1))
      }
    }
    "using foldMap" should {
      "with StringMonoid" in {
        ListFoldable.foldMap(List(1,2,3))(a => a.toString)(stringMonoid) should be("123")
      }
      "with intMonoid" in {
        ListFoldable.foldMap(List("aa", "a", "aaaa"))(a => a.length)(intAddition) should be(7)
        ListFoldable.foldMap(List("aa", "a", "aaaa"))(a => a.length)(intMultiplication) should be(8)
      }
    }
    "using concatenate" should {
      "with intMonoid" in {
        ListFoldable.concatenate(List(1, 2, 3, 4))(intAddition) should be(10)
        ListFoldable.concatenate(List(1, 2, 3, 4))(intMultiplication) should be(24)
      }
      "with stringMonoid" in {
        ListFoldable.concatenate(List("aa", "a", "aaaa"))(stringMonoid) should be("aaaaaaa")
      }
    }
  }
  "StreamFoldable" when {
    "using foldRight with a list" should {
      "return the same list" in {
        StreamFoldable.foldRight(LazyList(1, 2, 3))(List[Int]())((h, acc) => h :: acc) should be(List(1, 2, 3))
      }
    }
    "using foldLeft with a list" should {
      "return the same list in reverse order" in {
        StreamFoldable.foldLeft(LazyList(1, 2, 3))(List[Int]())((acc, h) => h :: acc) should be(List(3, 2, 1))
      }
    }
    "using foldMap" should {
      "with StringMonoid" in {
        StreamFoldable.foldMap(LazyList(1,2,3))(a => a.toString)(stringMonoid) should be("123")
      }
      "with intMonoid" in {
        StreamFoldable.foldMap(LazyList("aa", "a", "aaaa"))(a => a.length)(intAddition) should be(7)
        StreamFoldable.foldMap(LazyList("aa", "a", "aaaa"))(a => a.length)(intMultiplication) should be(8)
      }
    }
    "using concatenate" should {
      "with intMonoid" in {
        StreamFoldable.concatenate(LazyList(1, 2, 3, 4))(intAddition) should be(10)
        StreamFoldable.concatenate(LazyList(1, 2, 3, 4))(intMultiplication) should be(24)
      }
      "with stringMonoid" in {
        StreamFoldable.concatenate(LazyList("aa", "a", "aaaa"))(stringMonoid) should be("aaaaaaa")
      }
    }
  }
  "TreeFoldable" when {
    "using foldRight with a tree" should {
      "return the same tree" in {
        TreeFoldable.foldRight(Branch(Branch(Leaf(2), Leaf(1)), Leaf(3)))(Leaf(0):Tree[Int])((h, acc) => Branch(Leaf(h), acc)) should be(Branch(Leaf(2), Branch(Leaf(1), Branch(Leaf(3), Leaf(0)))))
      }
    }
    "using foldLeft with a tree" should {
      "return the same tree in reverse order" in {
        TreeFoldable.foldLeft(Branch(Branch(Leaf(2), Leaf(1)), Leaf(3)))(Leaf(0):Tree[Int])((acc, h) => Branch(Leaf(h), acc)) should be(Branch(Leaf(3), Branch(Leaf(1), Branch(Leaf(2),Leaf(0)))))
      }
    }
    "using foldMap" should {
      "with StringMonoid" in {
        TreeFoldable.foldMap(Branch(Branch(Leaf(2), Leaf(1)), Leaf(3)))(a => a.toString)(stringMonoid) should be("213")
      }
      "with intMonoid" in {
        TreeFoldable.foldMap(Branch(Branch(Leaf("aa"), Leaf("a")), Leaf("aaaa")))(a => a.length)(intAddition) should be(7)
        TreeFoldable.foldMap(Branch(Branch(Leaf("aa"), Leaf("a")), Leaf("aaaa")))(a => a.length)(intMultiplication) should be(8)
      }
    }
    "using concatenate" should {
      "with intMonoid" in {
        TreeFoldable.concatenate(Branch(Branch(Leaf(1), Leaf(2)), Branch(Leaf(3), Leaf(4))))(intAddition) should be(10)
        TreeFoldable.concatenate(Branch(Branch(Leaf(1), Leaf(2)), Branch(Leaf(3), Leaf(4))))(intMultiplication) should be(24)
      }
      "with stringMonoid" in {
        TreeFoldable.concatenate(Branch(Branch(Leaf("aa"), Leaf("a")), Leaf("aaaa")))(stringMonoid) should be("aaaaaaa")
      }
    }
  }
  "OptionFoldable" when {
    "using foldRight" should {
      "return the correct values" in {
        OptionFoldable.foldRight(Some(1))(None: Option[Int])((h, acc) => acc.map(b => h + b)) should be(None)
        OptionFoldable.foldRight(Some(1))(Some(1): Option[Int])((h, acc) => acc.map(b => h + b)) should be(Some(2))
        OptionFoldable.foldRight(None: Option[Int])(Some(1): Option[Int])((h, acc) => acc.map(b => h + b)) should be(Some(1))
        OptionFoldable.foldRight(None: Option[Int])(None: Option[Int])((h, acc) => acc.map(b => h + b)) should be(None)
      }
    }
    "using foldLeft" should {
      "return the correct values" in {
        OptionFoldable.foldLeft(Some(1))(None: Option[Int])((acc, h) => acc.map(b => h + b)) should be(None)
        OptionFoldable.foldLeft(Some(1))(Some(1): Option[Int])((acc, h) => acc.map(b => h + b)) should be(Some(2))
        OptionFoldable.foldLeft(None: Option[Int])(Some(1): Option[Int])((acc, h) => acc.map(b => h + b)) should be(Some(1))
        OptionFoldable.foldLeft(None: Option[Int])(None: Option[Int])((acc, h) => acc.map(b => h + b)) should be(None)
      }
    }
    "using foldMap" should {
      "with StringMonoid" in {
        OptionFoldable.foldMap(Some(2))(a => a.toString)(stringMonoid) should be("2")
        OptionFoldable.foldMap(None: Option[Int])(a => a.toString)(stringMonoid) should be("")
      }
      "with intMonoid" in {
        OptionFoldable.foldMap(Some("aaaaa"))(a => a.length)(intAddition) should be(5)
        OptionFoldable.foldMap(None: Option[String])(a => a.length)(intAddition) should be(0)
      }
    }
    "using concatenate" should {
      "with intMonoid" in {
        OptionFoldable.concatenate(Some(5))(intAddition) should be(5)
        OptionFoldable.concatenate(Some(5))(intMultiplication) should be(5)
      }
      "with stringMonoid" in {
        OptionFoldable.concatenate(Some("aaa"))(stringMonoid) should be("aaa")
      }
    }
  }
  "product monoid" when {
    "using the op" should {
      "satisfy the identity law" in {
        productMonoid(intAddition, intMultiplication).op((intAddition.zero, intMultiplication.zero), (24862, 24864)) should be((24862, 24864))
        productMonoid(intAddition, intMultiplication).op((24862, 24864), (intAddition.zero, intMultiplication.zero)) should be((24862, 24864))
      }
      "satisfy the associativity law" in {
        val composedMonoid = productMonoid(intAddition, intMultiplication)
        List((1, 1), (5, 5), (8974, 8974), (33, 33)).foldRight(composedMonoid.zero)(composedMonoid.op) should be((9013, 1480710))
        List((1, 1), (5, 5), (8974, 8974), (33, 33)).foldLeft(composedMonoid.zero)(composedMonoid.op) should be((9013, 1480710))
      }
    }
  }
  "function monoid" when {
    "using the op" should {
      val funcMonoid: Monoid[String => Int] = functionMonoid(intAddition)

      "satisfy the identity law" in {
        funcMonoid.op((a: String) => a.length, funcMonoid.zero)("aa") should be(2)
        funcMonoid.op(funcMonoid.zero, (a: String) => a.length)("aa") should be(2)
      }
      "satisfy the associativity law" in {
        List((a: String) => a.length, (a: String) => a.length * 2).foldRight(funcMonoid.zero)(funcMonoid.op)("asd") should be(9)
        List((a: String) => a.length, (a: String) => a.length * 2).foldLeft(funcMonoid.zero)(funcMonoid.op)("asd") should be(9)
      }
    }
  }
}
