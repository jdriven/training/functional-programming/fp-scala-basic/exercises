package fpinscala.generators

import fpinscala.chapter3._
import org.scalacheck.{Arbitrary, Gen}

object ArbitraryList {

  implicit def arbList[T](implicit a: Arbitrary[T]): Arbitrary[List[T]] =
    Arbitrary {

      def sizedList(sz: Int): Gen[List[T]] =
        if (sz <= 0) Gen.const(Nil)
        else
          for {
            e <- Arbitrary.arbitrary[T]
            rest <- sizedList(sz - 1)
          } yield Cons(e, rest)

      Gen.sized(sz => sizedList(sz))
    }
}
