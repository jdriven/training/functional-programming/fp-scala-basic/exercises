package fpinscala.generators

import fpinscala.chapter4._
import org.scalacheck.{Arbitrary, Gen}

object ArbitraryOption {

  implicit def arbOption[T](implicit a: Arbitrary[T]): Arbitrary[Option[T]] =
    Arbitrary {
      val genSome = for (e <- Arbitrary.arbitrary[T]) yield Some(e)
      Gen.oneOf(Gen.const(None), genSome)
    }
}
