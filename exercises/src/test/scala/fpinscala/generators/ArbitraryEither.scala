package fpinscala.generators

import fpinscala.chapter4._
import org.scalacheck.Arbitrary
import org.scalacheck.Arbitrary.arbitrary
import org.scalacheck.Gen.oneOf

object ArbitraryEither {
  implicit def arbEither[L, R](implicit a: Arbitrary[L], b: Arbitrary[R]): Arbitrary[Either[L, R]] =
    Arbitrary(oneOf(arbitrary[L].map(Left(_)), arbitrary[R].map(Right(_))))

  implicit def arbRight[R](implicit b: Arbitrary[R]): Arbitrary[Right[R]] =
    Arbitrary(arbitrary[R].map(Right(_)))
}
