package fpinscala.chapter3

import fpinscala.generators.ArbitraryTrees._
import fpinscala.generators.FPGeneratorConfiguration
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import org.scalatestplus.scalacheck.ScalaCheckDrivenPropertyChecks

class TreeSpec extends AnyWordSpecLike with Matchers with ScalaCheckDrivenPropertyChecks with FPGeneratorConfiguration {

  "A tree" when {

    import Tree._

    "the size" should {
      "combining two trees as one, adding the size of both trees should equal the size of the combined tree minus one" in {
        forAll("t1", "t2") { (t1: Tree[Unit], t2: Tree[Unit]) =>
          Tree.size(t1) + Tree.size(t2) should be(Tree.size(Branch(t1, t2)) - 1)
        }
      }
      "size of a single leaf should be 1" in {
        Tree.size(Leaf(0)) should be(1)
      }
    }

    "the max" should {
      "the max of two trees combined should be the max of the max of both trees" in {
        forAll("t1", "t2") { (t1: Tree[Int], t2: Tree[Int]) =>
          Math.max(maximum(t1), maximum(t2)) should be(maximum(Branch(t1, t2)))
        }
      }
      "the max of a single leaf should be that leaf's value" in {
        forAll("v") { v: Int => maximum(Leaf(v)) should be(v) }
      }
    }

    "the depth" should {
      "the depth of two trees combined should be the max of the depth of both subtrees, plus one" in {
        forAll("t1", "t2") { (t1: Tree[Unit], t2: Tree[Unit]) =>
          depth(Branch(t1, t2)) should be(Math.max(depth(t1), depth(t2)) + 1)
        }
      }
      "the depth of a single leaf should be 0" in {
        depth(Leaf(0)) should be(0)
      }
    }

    "the map" should {
      val f: Int => String = el => el.toString
      "mapping of two trees combined should be the result of combining both trees and mapping over it" in {
        forAll("t1", "t2") { (t1: Tree[Int], t2: Tree[Int]) =>
          Branch(map(t1)(f), map(t2)(f)) should be(map(Branch(t1, t2))(f))
        }
      }
      "not change the structure of the tree" in {
        forAll("t") { t: Tree[Int] =>
          map(t)(identity) should be(t)
          depth(t) should be(depth(map(t)(f)))
          Tree.size(t) should be(Tree.size(map(t)(f)))
        }
      }
    }

    "the sizeViaFold" should {
      "combining two trees as one, adding the size of both trees should equal the size of the combined tree minus one" in {
        forAll("t1", "t2") { (t1: Tree[Unit], t2: Tree[Unit]) =>
          Tree.sizeViaFold(t1) + Tree.sizeViaFold(t2) should be(Tree.sizeViaFold(Branch(t1, t2)) - 1)
        }
      }
      "size of a single leaf should be 1" in {
        Tree.sizeViaFold(Leaf(0)) should be(1)
      }
    }

    "the maximumViaFold" should {
      "the max of two trees combined should be the max of the max of both trees" in {
        forAll("t1", "t2") { (t1: Tree[Int], t2: Tree[Int]) =>
          Math.max(maximumViaFold(t1), maximumViaFold(t2)) should be(maximumViaFold(Branch(t1, t2)))
        }
      }
      "the max of a single leaf should be that leaf's value" in {
        forAll("v") { v: Int => maximumViaFold(Leaf(v)) should be(v) }
      }
    }

    "the depthViaFold" should {
      "the depth of two trees combined should be the max of the depth of both subtrees, plus one" in {
        forAll("t1", "t2") { (t1: Tree[Unit], t2: Tree[Unit]) =>
          depthViaFold(Branch(t1, t2)) should be(Math.max(depthViaFold(t1), depthViaFold(t2)) + 1)
        }
      }
      "the depth of a single leaf should be 0" in {
        depthViaFold(Leaf(0)) should be(0)
      }
    }

    "the mapViaFold" should {
      val f: Int => String = el => el.toString
      "mapping of two trees combined should be the result of combining both trees and mapping over it" in {
        forAll("t1", "t2") { (t1: Tree[Int], t2: Tree[Int]) =>
          Branch(mapViaFold(t1)(f), mapViaFold(t2)(f)) should be(mapViaFold(Branch(t1, t2))(f))
        }
      }
      "not change the structure of the tree" in {
        forAll("t") { t: Tree[Int] =>
          mapViaFold(t)(identity) should be(t)
          depthViaFold(t) should be(depthViaFold(mapViaFold(t)(f)))
          Tree.sizeViaFold(t) should be(Tree.sizeViaFold(mapViaFold(t)(f)))
        }
      }
    }
  }
}
