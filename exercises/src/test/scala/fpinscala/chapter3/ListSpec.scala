package fpinscala.chapter3

import List._
import fpinscala.generators.FPGeneratorConfiguration
import fpinscala.generators.ArbitraryList._
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import org.scalatestplus.scalacheck.ScalaCheckDrivenPropertyChecks

class ListSpec extends AnyWordSpecLike with Matchers with ScalaCheckDrivenPropertyChecks with FPGeneratorConfiguration {

  override implicit val generatorDrivenConfig: PropertyCheckConfiguration =
    PropertyCheckConfiguration(minSuccessful = 200, sizeRange = 10)

  "the tail function" when {
    "given a empty list" should {
      "return an empty list" in {
        tail(Nil) should be(Nil)
      }
    }

    "given a list with one element" should {
      "return an empty list" in {
        tail(List(1)) should be(Nil)
      }
    }

    "given a list with multiple elements" should {
      "return the list without it's first element" in {
        tail(List(1, 2)) should be(List(2))
        tail(List(143, 2234, 433)) should be(List(2234, 433))
      }
    }
  }

  "the setHead function" when {
    "given a empty list" should {
      "return a one element list on setHead" in {
        setHead(List(), 1) should be(List(1))
      }
    }

    "given a list with one element" should {
      "return a list with the head replaced on setHead" in {
        setHead(List(1), 2) should be(List(2))
      }
    }

    "given a list with multiple elements" should {
      "return a list with the head replaced on setHead" in {
        setHead(List(1, 2), 2) should be(List(2, 2))
      }
    }
  }

  "the drop function" when {
    "given a empty list" should {
      "n < 0" should {
        "return an empty list" in {
          drop(List(), -1) should be(Nil)
        }
      }

      "n == 0" should {
        "return an empty list" in {
          drop(List(), 0) should be(Nil)
        }
      }

      "n > 0" should {
        "return an empty list" in {
          drop(List(), 1) should be(Nil)
        }
      }
    }

    "given a list with one element" should {
      "n < 0" should {
        "return the list" in {
          drop(List(1), -1) should be(List(1))
        }
      }

      "n == 0" should {
        "return the list" in {
          drop(List(1), 0) should be(List(1))
        }
      }

      "n > 0" should {
        "return an empty list" in {
          drop(List(1), 1) should be(Nil)
        }
      }
    }

    "given a list with multiple elements" should {
      "n < 0" should {
        "return the list" in {
          drop(List(1, 2), -1) should be(List(1, 2))
        }
      }

      "n == 0" should {
        "return the list" in {
          drop(List(1, 2), 0) should be(List(1, 2))
        }
      }

      "n > 0" should {
        "return the list with the first n removed" in {
          drop(List(1, 2), 1) should be(List(2))
          drop(List(1, 2, 3, 4), 2) should be(List(3, 4))
        }
      }
    }
  }

  "the dropWhile function" when {
    "given a empty list" should {
      "return an empty list" in {
        dropWhile(List(), (a: Int) => a < 4) should be(Nil)
      }
    }

    "given a list with one element" should {
      "return an empty list if the predicate matches" in {
        dropWhile(List(1), (a: Int) => a < 4) should be(Nil)
      }

      "return the list if the predicate doesn't match" in {
        dropWhile(List(1), (a: Int) => a < 1) should be(List(1))
      }
    }

    "given a list with multiple elements" should {
      "return an empty list if the predicate matches all elements" in {
        dropWhile(List(1, 2, 3), (a: Int) => a < 4) should be(Nil)
      }

      "return the list if the predicate doesn't match any element" in {
        dropWhile(List(1, 2, 3), (a: Int) => a < 1) should be(List(1, 2, 3))
      }

      "return part of the list if the predicate matches some elements" in {
        dropWhile(List(1, 2, 3, 4, 5, 6), (a: Int) => a < 4) should be(List(4, 5, 6))
      }
    }
  }

  "the init function" when {
    "given an empty list" should {
      "return an empty list" in {
        init(List()) should be(Nil)
      }
    }

    "given a list with one element" should {
      "return an empty list" in {
        init(List(1)) should be(Nil)
      }
    }

    "given a list with multiple elements" should {
      "return the list with the last element removed" in {
        init(List(1, 2, 3, 4)) should be(List(1, 2, 3))
      }
    }
  }

  "the length function" should {
    "empty list should have length 0" in (List.length(Nil) should be(0))
    "1 + length(l1) should be the same as length(Cons(d, l1))" in {
      forAll("l1", "d") { (l1: List[Int], d: Int) =>
        1 + List.length(l1) should be(List.length(Cons(d, l1)))
      }
    }
  }

  "the foldLeft function" should {
    "given a list" should {
      "return list in the reverse order" in {
        foldLeft(List(1, 2, 3), List[Int]())((acc, h) => Cons(h, acc)) should be(List(3, 2, 1))
      }
    }
  }

  "the sumFL function" should {
    "empty list should sum to 0" in (sumFL(Nil) should be(0))
    "d + sum(l1) should be the same as sum(Cons(d, l1))" in {
      forAll("l1", "d") { (l1: List[Int], d: Int) =>
        d + List.sumFL(l1) should be(sumFL(Cons(d, l1)))
      }
    }
  }

  "the productFL function" should {
    "empty list should product to 0" in (productFL(Nil) should be(1.0))
    "d * productFL(l1) should be the same as productFL(Cons(d, l1))" in {
      forAll("l1", "d") { (l1: List[Int], d: Int) =>
        d * List.productFL(l1) should be(productFL(Cons(d, l1)))
      }
    }
  }

  "the lengthFL function" should {
    "empty list should have length 0" in (lengthFL(Nil) should be(0))
    "1 + lengthFL(l1) should be the same as lengthFL(Cons(d, l1))" in {
      forAll("l1", "d") { (l1: List[Int], d: Int) =>
        1 + lengthFL(l1) should be(lengthFL(Cons(d, l1)))
      }
    }
  }

  "the reverse function" should {
    "empty reverse should be Nil" in (List.reverse(Nil) should be(Nil))
    "reverse of reverse should be the same list" in {
      forAll("l1") { l1: List[Int] => List.reverse(List.reverse(l1)) should be(l1) }
    }
    "for a list a and b, append(reverse(l1), reverse(l2)) should be reverse(append(l2, l1))" in {
      forAll("l1", "l2") { (l1: List[Int], l2: List[Int]) =>
        List.append(List.reverse(l1), List.reverse(l2)) should be(List.reverse(List.append(l2, l1)))
      }
    }
  }

  "the append function" should {
    "for a list a and b, appendFL(l1, l2) should be append(l1, l2))" in {
      forAll("l1", "l2") { (l1: List[Int], l2: List[Int]) =>
        List.appendFL(l1, l2) should be(List.append(l1, l2))
      }
    }
    "for a list a and b, appendFR(l1, l2) should be append(l1, l2))" in {
      forAll("l1", "l2") { (l1: List[Int], l2: List[Int]) =>
        List.appendFR(l1, l2) should be(List.append(l1, l2))
      }
    }
  }

  "the concat function" should {
    "for a list of lists, should return a list with all the values combined" in {
      List.concat(List(List(1, 2), List(3), List(4, 5))) should be(List(1, 2, 3, 4, 5))
      List.concat(List(List("a", "b"), List("c"), List("d", "e"))) should be(List("a", "b", "c", "d", "e"))
    }
  }

  "the add1 function" should {
    "for a list of numbers, should return a list with all the values + 1" in {
      List.add1(List(1, 2, 6, 337, 11)) should be(List(2, 3, 7, 338, 12))
    }
  }

  "the doubleToString function" should {
    "for a list of doubles, should return a list with all string values of those doubles" in {
      List.doubleToString(List(1.22, 4.548, 3.14)) should be(List("1.22", "4.548", "3.14"))
    }
  }

  "the map function" should {
    "for a list of A's return a List of B's using the given function" in {
      List.map(List(7, 33, 80))((i: Int) => i * 28) should be(List(196, 924, 2240))
    }
  }

  "the map_1 function" should {
    "for a list of A's return a List of B's using the given function" in {
      List.map_1(List(7, 33, 80))((i: Int) => i * 28) should be(List(196, 924, 2240))
    }
  }

  "the filter function" should {
    "for a list of A's, return a List of A's with the values filtered by the given function" in {
      List.filter(List(1, 2, 3, 4, 5, 6))((i: Int) => i < 4) should be(List(1, 2, 3))
    }
  }

  "the flatmap function" should {
    "adhere to the Monad laws" should {
      "Left identity: List(x) flatMap f == f(x)" in {
        forAll("x", "f") { (x: Int, f: Int => List[Int]) =>
          List.flatMap(Cons(x, Nil))(f) should be(f(x))
        }
      }
      "Right identity: (m flatMap List(_) === m" in {
        forAll("m") { m: List[Int] =>
          List.flatMap(m)(Cons(_, Nil)) should be(m)
        }
      }
      "Associativity: (m flatMap f) flatMap g === m flatMap { x => f(x) flatMap g }" in {
        forAll("m", "f", "g") { (m: List[Int], f: Int => List[Int], g: Int => List[Int]) =>
          flatMap(flatMap(m)(f))(g) should be(flatMap(m) { x => flatMap(f(x))(g) })
        }
      }
    }

    "the filterViaFlatMap function" should {
      "for a list of A's, return a List of A's with the values filtered by the given function" in {
        List.filterViaFlatMap(List(1, 2, 3, 4, 5, 6))((i: Int) => i < 4) should be(List(1, 2, 3))
      }
    }

    "the addPairwise function" should {
      "combine two integer lists by adding the values with each other" in {
        List.addPairwise(List(1, 2, 3), List(4, 5, 6)) should be(List(5, 7, 9))
        List.addPairwise(List(5, 6, 9, 6), List(4, 5, 6)) should be(List(9, 11, 15))
        List.addPairwise(List(4, 5, 8), List(4, 5, 6, 7)) should be(List(8, 10, 14))
      }
    }

    "the zipWith function" should {
      "combine two lists using the given function" in {
        List.zipWith(List(1, 2, 3), List(4, 5, 6))(_+_) should be(List(5, 7, 9))
        List.zipWith(List(5, 6, 9, 6), List(4, 5, 6))(_+_) should be(List(9, 11, 15))
        List.zipWith(List(4, 5, 8), List(4, 5, 6, 7))(_+_) should be(List(8, 10, 14))
        List.zipWith(List("a", "b"), List("c", "d"))(_+_) should be(List("ac", "bd"))
      }
    }

    "the hasSubsequence function" should {
      "return true if the subsequence is in the list" in {
        List.hasSubsequence(List(1, 2, 3, 4, 5), List(3, 4)) should be(true)
        List.hasSubsequence(List("a", "b", "c", "d", "e"), List("e")) should be(true)
      }

      "return false if the subsequence is not in the list" in {
        List.hasSubsequence(List(1, 2, 3, 4, 5), List(3, 4, 6)) should be(false)
        List.hasSubsequence(List("a", "b", "c", "d", "e"), List("u")) should be(false)
      }
    }
  }
}
