package fpinscala.chapter5

import Stream._

import scala.annotation.tailrec

trait Stream[+A] {

  def exists(p: A => Boolean): Boolean = foldRight(false)((a, b) => p(a) || b) // Here `b` is the unevaluated recursive step that folds the tail of the stream. If `p(a)` returns `true`, `b` will never be evaluated and the computation terminates early.

  @annotation.tailrec
  final def find(f: A => Boolean): Option[A] = this match {
    case Empty => None
    case Cons(h, t) => if (f(h())) Some(h()) else t().find(f)
  }

  def headOption: Option[A] = ???

  // Helper method for tests
  override def equals(obj: Any): Boolean = obj.isInstanceOf[Stream[A]] && Stream.strEquals(this, obj.asInstanceOf[Stream[A]])

  // Helper method for tests
  final override def toString: String = {
    @tailrec
    def tailRecToString(acc: String, s: Stream[A]): String = {
      s match {
        case Empty => acc + "]"
        case Cons(h1, t1) => tailRecToString(acc + ", " + h1(), t1())
      }
    }

    tailRecToString("[", this)
  }

  // TODO Exercise 5.1
  /**
    * Force the evaluation of the stream, and return the stream as a list.
    */
  def toList: List[A] = ???

  // TODO Exercise 5.2
  /**
    * Take the first 'n' elements of the stream and return a new stream.
    */
  def take(n: Int): Stream[A] = ???

  /**
    * Drop the first 'n' elements of the stream and return a new stream.
    */
  def drop(n: Int): Stream[A] = ???

  // TODO Exercise 5.3
  /**
    * Takes the elements from the stream as long as the condition is true.
    */
  def takeWhile(p: A => Boolean): Stream[A] = ???

  // Text 5.3
  // The arrow `=>` in front of the argument type `B` means that the function `f` takes its second argument by name and may choose not to evaluate it.
  // If `f` doesn't evaluate its second argument, the recursion never occurs.
  def foldRight[B](z: => B)(f: (A, => B) => B): B = this match {
    case Cons(h, t) => f(h(), t().foldRight(z)(f))
    case _ => z
  }

  def exists2(p: A => Boolean): Boolean = foldRight(false)((a, b) => p(a) || b) // Here `b` is the unevaluated recursive step that folds the tail of the stream. If `p(a)` returns `true`, `b` will never be evaluated and the computation terminates early.

  // TODO Exercise 5.4
  def forAll(f: A => Boolean): Boolean = ???

  // TODO Exercise 5.5
  def takeWhile2(f: A => Boolean): Stream[A] = ???

  // TODO Exercise 5.6
  def headOption2: Option[A] = ???

  // TODO Exercise 5.7
  def map[B](f: A => B): Stream[B] = ???

  def filter(f: A => Boolean): Stream[A] = ???

  def append[B >: A](other: Stream[B]): Stream[B] = ???

  def flatMap[B](f: A => Stream[B]): Stream[B] = ???

  //map, take, takeWhile, zipWith
  // TODO Exercise 5.13
  def map2[B](f: A => B): Stream[B] = ???

  def takeWhile3(f: A => Boolean): Stream[A] = ???

  // special case of `zipWith`
  def zip[B](s2: Stream[B]): Stream[(A, B)] = ???

  def zipWith[B, C](s2: Stream[B])(f: (A, B) => C): Stream[C] = ???

  // Probably better way to write this
  def zipAll[B](s2: Stream[B]): Stream[(Option[A], Option[B])] = ???

  // TODO Exercise 5.14
  def startsWith[A](s: Stream[A]): Boolean = ???

  // TODO Exercise 5.15
  def tails: Stream[Stream[A]] = ???


  def hasSubsequence[A](s: Stream[A]): Boolean = ???

  /*
  The function can't be implemented using `unfold`, since `unfold` generates elements of the `Stream` from left to right. It can be implemented using `foldRight` though.
  The implementation is just a `foldRight` that keeps the accumulated value and the stream of intermediate results, which we `cons` onto during each iteration. When writing folds, it's common to have more state in the fold than is needed to compute the result. Here, we simply extract the accumulated list once finished.
  */
  def scanRight[B](z: => B)(f: (A, => B) => B): Stream[B] = ???

  def sliding(n: Int): Stream[Stream[A]] = tails.map(_.take(n))

  def last: Option[A] = foldRight[Option[A]](None)((a, _) => Some(a))
}

case object Empty extends Stream[Nothing]

case class Cons[+A](h: () => A, t: () => Stream[A]) extends Stream[A]

object Stream {

  def cons[A](hd: => A, tl: => Stream[A]): Stream[A] = {
    lazy val head = hd
    lazy val tail = tl
    Cons(() => head, () => tail)
  }

  def empty[A]: Stream[A] = Empty

  def apply[A](as: A*): Stream[A] = if (as.isEmpty) empty else cons(as.head, apply(as.tail: _*))

  // Helper method for tests
  @tailrec
  final def strEquals[B](left: Stream[B], right: Stream[B]): Boolean = {
    (left, right) match {
      case (_: Empty.type, _: Empty.type) => true
      case (l: Cons[B], r: Cons[B]) if l.h() == r.h() => strEquals(l.t(), r.t())
      case _ => false
    }
  }

  // Text 5.4
  def ones: Stream[Int] = cons(1, ones)

  // TODO Exercise 5.8
  def constant[A](a: A): Stream[A] = ???

  // TODO Exercise 5.9
  def from(n: Int): Stream[Int] = ???

  // TODO Exercise 5.10
  def fibs: Stream[Int] = ???

  // TODO Exercise 5.11
  def unfold[A, S](z: S)(f: S => Option[(A, S)]): Stream[A] = ???

  // TODO Exercise 5.12
  // Write fibs, constant, and ones in terms of unfold
  def fibs2: Stream[Int] = ???

  def constant2[A](a: A): Stream[A] = ???

  def from2(n: Int): Stream[Int] = ???

  def ones2: Stream[Int] = ???
}
