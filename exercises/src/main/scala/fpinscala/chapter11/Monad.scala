package fpinscala.chapter11

import fpinscala.chapter11.Monad._

trait Functor[F[_]] {
  def map[A, B](fa: F[A])(f: A => B): F[B]

  def distribute[A, B](fab: F[(A, B)]): (F[A], F[B]) =
    (map(fab)(_._1), map(fab)(_._2))

  def codistribute[A, B](e: Either[F[A], F[B]]): F[Either[A, B]] = e match {
    case Left(fa) => map(fa)(Left(_))
    case Right(fb) => map(fb)(Right(_))
  }
}

object Functor {
  val listFunctor = new Functor[List] {
    def map[A, B](as: List[A])(f: A => B): List[B] = as map f
  }
}

trait Monad[M[_]] extends Functor[M] {
  def unit[A](a: => A): M[A]

  def flatMap[A, B](ma: M[A])(f: A => M[B]): M[B]

  def map[A, B](ma: M[A])(f: A => B): M[B] =
    flatMap(ma)(a => unit(f(a)))

  def map2[A, B, C](ma: M[A], mb: M[B])(f: (A, B) => C): M[C] =
    flatMap(ma)(a => map(mb)(b => f(a, b)))

  // TODO Exercise 11.3
  def sequence[A](lma: List[M[A]]): M[List[A]] = ???

  // TODO Exercise 11.3
  def traverse[A, B](la: List[A])(f: A => M[B]): M[List[B]] = ???

  // TODO Exercise 11.4 & 11.5
  def replicateM[A](n: Int, ma: M[A]): M[List[A]] = ???

  // TODO Exercise 11.6
  def filterM[A](ms: List[A])(f: A => M[Boolean]): M[List[A]] = ???

  // TODO Exercise 11.7
  def compose[A, B, C](f: A => M[B], g: B => M[C]): A => M[C] = ???

  // TODO Exercise 11.8
  // Implement in terms of `compose`:
  def _flatMap[A, B](ma: M[A])(f: A => M[B]): M[B] = ???

  // TODO Exercise 11.12
  def join[A](mma: M[M[A]]): M[A] = ???

  // TODO Exercise 11.13
  // Implement in terms of `join`:
  def __flatMap[A, B](ma: M[A])(f: A => M[B]): M[B] = ???

  def __compose[A, B, C](f: A => M[B], g: B => M[C]): A => M[C] = ???
}

// TODO Exercise 11.20
case class Reader[R, A](run: R => A) {
  def map[B](f: A => B): Reader[R, B] = readerMonad[R].map(this)(f)
  def flatMap[B](f: A => Reader[R, B]): Reader[R, B] = readerMonad[R].flatMap(this)(f)
}

object Monad {
  // TODO Exercise 11.1
  lazy val optionMonad: Monad[Option] = new Monad[Option] {
    override def unit[A](a: => A): Option[A] = ???

    override def flatMap[A, B](ma: Option[A])(f: A => Option[B]): Option[B] = ???
  }

  // TODO Exercise 11.1
  lazy val streamMonad: Monad[LazyList] = ???

  // TODO Exercise 11.1
  lazy val listMonad: Monad[List] = ???

  // TODO Exercise 11.17
  lazy val idMonad: Monad[Id] = ???

  // TODO Exercise 11.20
  def readerMonad[R]: Monad[Reader[R, *]] = new Monad[Reader[R, *]] {
    override def unit[A](a: => A): Reader[R, A] = ???

    override def flatMap[A, B](ma: Reader[R, A])(f: A => Reader[R, B]): Reader[R, B] = ???
  }
}

case class Id[A](value: A) {
  def map[B](f: A => B): Id[B] = idMonad.map(this)(f)
  def flatMap[B](f: A => Id[B]): Id[B] = idMonad.flatMap(this)(f)
}
