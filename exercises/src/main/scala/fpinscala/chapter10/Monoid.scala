package fpinscala.chapter10

trait Monoid[A] {
  def op(a1: A, a2: A): A

  def zero: A
}

object Monoid {

  val stringMonoid = new Monoid[String] {
    def op(a1: String, a2: String) = a1 + a2

    val zero = ""
  }

  def listMonoid[A] = new Monoid[List[A]] {
    def op(a1: List[A], a2: List[A]) = a1 ++ a2

    val zero = Nil
  }

  // TODO: Exercise 10.1
  def intAddition: Monoid[Int] = ???

  def intMultiplication: Monoid[Int] = ???

  def booleanOr: Monoid[Boolean] = ???

  def booleanAnd: Monoid[Boolean] =  ???

  // TODO: Exercise 10.2
  def optionMonoid[A]: Monoid[Option[A]] = ???

  // TODO: Exercise 10.3
  def endoMonoid[A]: Monoid[A => A] = ???

  // We can get the dual of any monoid just by flipping the `op`.
  def dual[A](m: Monoid[A]): Monoid[A] = new Monoid[A] {
    def op(x: A, y: A): A = m.op(y, x)
    val zero = m.zero
  }

  // TODO: Exercise 10.5
  def foldMap[A, B](as: List[A], m: Monoid[B])(f: A => B): B = ???

  // TODO: Exercise 10.6
  def foldRight[A, B](as: List[A])(z: B)(f: (A, B) => B): B = ???

  def foldLeft[A, B](as: List[A])(z: B)(f: (B, A) => B): B = ???

  sealed trait WC
  case class Stub(chars: String) extends WC
  case class Part(lStub: String, words: Int, rStub: String) extends WC

  // TODO: Exercise 10.10
  def wcMonoid: Monoid[WC] = ???

  // TODO: Exercise 10.11
  def count(s: String): Int = ???

  // TODO: Exercise 10.16
  def productMonoid[A, B](A: Monoid[A], B: Monoid[B]): Monoid[(A, B)] = ???

  // TODO: Exercise 10.17
  def functionMonoid[A, B](B: Monoid[B]): Monoid[A => B] = ???

  def mapMergeMonoid[K, V](V: Monoid[V]): Monoid[Map[K, V]] = new Monoid[Map[K, V]] {
      def zero = Map[K, V]()

      def op(a: Map[K, V], b: Map[K, V]) =
        (a.keySet ++ b.keySet).foldLeft(zero) { (acc, k) =>
          acc.updated(k, V.op(a.getOrElse(k, V.zero), b.getOrElse(k, V.zero)))
        }
    }
}

trait Foldable[F[_]] {
  import Monoid._

  def foldRight[A, B](as: F[A])(z: B)(f: (A, B) => B): B = ???
  def foldLeft[A, B](as: F[A])(z: B)(f: (B, A) => B): B = ???
  def foldMap[A, B](as: F[A])(f: A => B)(mb: Monoid[B]): B = ???
  def concatenate[A](as: F[A])(m: Monoid[A]): A = ???
}

// TODO: Exercise 10.12.2
object ListFoldable extends Foldable[List] {
  override def foldRight[A, B](as: List[A])(z: B)(f: (A, B) => B): B = ???
  override def foldLeft[A, B](as: List[A])(z: B)(f: (B, A) => B): B = ???
  override def foldMap[A, B](as: List[A])(f: A => B)(mb: Monoid[B]): B = ???
}

// TODO: Exercise 10.12.3
object StreamFoldable extends Foldable[LazyList] {
  override def foldRight[A, B](as: LazyList[A])(z: B)(f: (A, B) => B): B = ???
  override def foldLeft[A, B](as: LazyList[A])(z: B)(f: (B, A) => B): B = ???
}

sealed trait Tree[+A]
case class Leaf[A](value: A) extends Tree[A]
case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]

// TODO: Exercise 10.13
object TreeFoldable extends Foldable[Tree] {
  override def foldMap[A, B](as: Tree[A])(f: A => B)(mb: Monoid[B]): B = ???
  override def foldLeft[A, B](as: Tree[A])(z: B)(f: (B, A) => B): B = ???
  override def foldRight[A, B](as: Tree[A])(z: B)(f: (A, B) => B): B = ???
}

// TODO: Exercise 10.14
object OptionFoldable extends Foldable[Option] {
  override def foldMap[A, B](as: Option[A])(f: A => B)(mb: Monoid[B]): B = ???
  override def foldLeft[A, B](as: Option[A])(z: B)(f: (B, A) => B): B = ???
  override def foldRight[A, B](as: Option[A])(z: B)(f: (A, B) => B): B = ???
}
