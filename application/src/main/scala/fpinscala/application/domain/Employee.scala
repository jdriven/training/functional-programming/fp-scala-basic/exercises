package fpinscala.application.domain

import upickle.default.{ReadWriter, macroRW}

/**
  * This class remains unchanged
  */
case class Employee(firstName: String, lastName: String)

object Employee {
  implicit val rw: ReadWriter[Employee] = macroRW
}
