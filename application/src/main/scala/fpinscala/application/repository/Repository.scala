package fpinscala.application.repository

import com.typesafe.scalalogging.LazyLogging
import fpinscala.application.domain.Employee
import fpinscala.application.repository.Repository.Queries

import java.sql.{Connection, DriverManager, ResultSet}
import scala.annotation.tailrec
import scala.collection.mutable
import scala.util.Try

//TODO 8 => make the constructor private by adding the keyword between the class name and the parameters
// Repository can now be created without the new keyword (i.e. Repository()) which calls Repository.apply)
class Repository(con: Connection) extends LazyLogging {

  //TODO 18 the template for TODO 17
  //TODO 19 => use syntactic sugar and shorten [[employee => employee]] with the shorthand [[_]]
  //TODO 23 => fix compilation errors here by adding the right filter column.
  //TODO 30 => change the return type to reflect your changes
  //Ignore 'lazy', its a workaround until we cover this keyword in a later session.
  private lazy val getEmployeeByFirstName: String => Employee = employee => ???
  // Ignore 'lazy', its a workaround until we cover this keyword in a later session.
  private lazy val getEmployeeByLastName: String => Employee = employee => ???

  //TODO 16:
  // => clean up the method body: remove returns, method curly braces and if/else -> pattern matching
  // NB [[null]] can be matched on as well!
  //
  //TODO 17:
  // => partially apply [[getEmployeeBy]] to extract 2 functions into the template fields at 8.2.2
  // => and reuse them in this method
  //
  //TODO 31: use pattern matching to cope with Try[Employee] and return Try[Employee]
  def getEmployee(employee: String): Employee = {
    val byFirstName = getEmployeeBy(Queries.selectEmployeeByFirstName, employee)
    if (byFirstName == null) return getEmployeeBy(Queries.selectEmployeeByLastName, employee)
    else return byFirstName
  }

  //TODO 10 => try/catch is an expression, it doesn't need return statements. Clean it up.
  //TODO 11 => a single expression method doesn't need curly braces. Remove them.
  //TODO 15 => Rewrite the try/catch expression to use Try and pattern match it for the result
  //TODO 22
  // => Curry this method between [[query]] and [[employee]], so partial application becomes cleaner. Fix compilation errors.
  // => Contemplate about what you have done and what the benefit is at the callng sites
  //TODO 29 => let this method return Try[Employee] and fix compilation errors
  private def getEmployeeBy(query: String, employee: String): Employee = {
    try {
      val stm = con.prepareStatement(query)
      stm.setString(1, employee.capitalize)
      val resultSet = stm.executeQuery()
      resultSet.next()
      return Employee(resultSet.getString("first_name"), resultSet.getString("last_name"))
    } catch {
      case e: Exception =>
        logger.error(s"Error finding $employee", e)
        return null
    }
  }

  //TODO 14:
  // Try is an ADT/sealed trait of a Success(value) or Failure(exception)
  // ADTs can be pattern matched to access their inner values.
  // => replace the if expression with a pattern match
  //
  //TODO 24 => fix compilation errors here by adding the right filter column.
  //TODO: 10 => reuse [[getEmployee]] instead of the map body
  //
  //TODO 33: turn the List[Try[Employee]] into a Try[List[Employee]]
  // => start with employee.map(getEmployee).foldLeft
  // => think about the zero: Try[List[Employee]]
  // => write out all the cases in the mapping function (Try[List[Employee]], Try[Employee]) => Try[List[Employee]]
  // If done correctly, you now changed the meaning of our application. It will fail the full method whenever one name is not found!
  // => This fails a unit test, fix it!
  //
  // TODO 34 => you may now as well return a Try[List[Employee]], to make handling errors easier in [[fpinscala.application.api.Routes]]
  def getEmployees(employees: List[String]): List[Employee] = employees.map { employee =>
    val maybeEmployee = Try {
      val stm = con.prepareStatement(Queries.selectEmployeeByFirstName)
      stm.setString(1, employee.capitalize)
      val resultSet = stm.executeQuery()
      resultSet.next()
      Employee(resultSet.getString("first_name"), resultSet.getString("last_name"))
    }

    if (maybeEmployee.isSuccess) { maybeEmployee.get } else {
      //TODO 13 => use string interpolation instead of concatenation
      logger.error("Error finding " + employee, maybeEmployee.failed.get)
      null
    }
  }

  //TODO 25 => replace the while loop with the given recursive helper method template
  def getEmployees(): List[Employee] = {

    //TODO 26:
    // => make this method generic and provide a mapper function that creates your type from a [[ResultSet]]
    // => make this mapper function a curried argument to improve type inference at the calling site!
    //
    @tailrec
    def untilExhausted(resultSet: ResultSet, acc: List[Employee]): List[Employee] = {
      ???
      untilExhausted(???, ???)
    }

    try {
      val results   = mutable.ListBuffer[Employee]()
      val stm       = con.prepareStatement(Queries.selectEmployees)
      val resultSet = stm.executeQuery()
      while (resultSet.next()) {
        results.addOne(Employee(resultSet.getString("first_name"), resultSet.getString("last_name")))
      }
      results.toList
    } catch {
      case e: Exception =>
        logger.error(s"Error finding employees", e)
        List()
    }
  }
}

//TODO 6:
// this is the companion object to class Repository.
// It holds static fields and methods that can be accessed by [[Repository.<method/field>]]
// Remember this information and continue.
object Repository {
  private val DATABASE_DIR: String = "my-h2-db"
  //TODO 12 => use string interpolation instead of concatenation
  private val DATABASE_URL: String = "jdbc:h2:mem:" + DATABASE_DIR

  lazy val con: Connection = DriverManager.getConnection(DATABASE_URL)

  //TODO 7:
  // => turn this block into the body of a [[def apply()]] method that returns new [[Repository(con)]]
  // this is syntactic sugar for a factory method that by default is used by case classes e.g. [[fpinscala.application.domain.Employee]]
  {
    val table: String = "CREATE TABLE employees(id int auto_increment, first_name varchar(64), last_name varchar(64));"

    val insert = "INSERT INTO employees(first_name, last_name) values (?, ?);"
    val employees = List(
      Employee("Paul", "Elstak"),
      Employee("Mental", "Theo"),
      Employee("Captain", "Jack"),
      Employee("T", "Spoon"),
      Employee("2", "Unlimited"),
      Employee("Backstreet", "Boys"),
      Employee("Spice", "Girls"),
      Employee("Britney", "Spears"),
      Employee("Christina", "Aguilera"),
      Employee("Ricky", "Martin")
    )

    con.createStatement().execute(table)
    val insertEmployee = con.prepareStatement(insert)
    employees.foreach {
      case Employee(f, l) =>
        insertEmployee.setString(1, f)
        insertEmployee.setString(2, l)
        insertEmployee.execute()
        insertEmployee.clearParameters()
    }
  }

  private object Queries {
    // TODO 21 the template for TODO 20
    // Ignore 'lazy', its a workaround until we cover this keyword in a later session.
    lazy val selectEmployeeBy: String => String = ???

    //TODO 20:
    // => merge these into one [[val selectEmployeeBy: String => String]] that takes the WHERE filter column.
    // => Use string interpolation.
    // => Remove this variable in favor of selectEmployeeBy.
    val selectEmployeeByFirstName: String =
      """
        |SELECT * FROM employees
        |WHERE first_name = ?
        |LIMIT 1;
        |""".stripMargin

    //TODO 20:
    // => merge these into one [[val selectEmployeeBy: String => String]] that takes the WHERE filter column.
    // => Use string interpolation.
    // => Remove this variable in favor of selectEmployeeBy.
    val selectEmployeeByLastName: String =
      """
        |SELECT * FROM employees
        |WHERE last_name = ?
        |LIMIT 1;
        |""".stripMargin

    val selectEmployees: String =
      """
        |SELECT * FROM employees;
        |""".stripMargin
  }
}
