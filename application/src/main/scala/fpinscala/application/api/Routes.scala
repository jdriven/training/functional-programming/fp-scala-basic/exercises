package fpinscala.application.api

import cask.model.Response
import com.typesafe.scalalogging.LazyLogging
import fpinscala.application.domain.Employee
import fpinscala.application.repository.Repository

class Routes private (repository: Repository) extends cask.MainRoutes with LazyLogging {

  //TODO 27: Remove the curly braces
  @cask.get("/")
  def hello() = {
    "Hello World!"
  }

  //TODO 28 => Null can also be matched on! Replace the if statement with a pattern match.
  //TODO 32 => use pattern matching to cope with Try[Employee] instead of [[null]]
  @cask.getJson("/employee/:name")
  def getEmployee(name: String): Response[Employee] = {
    logger.info(name)
    val employee = repository.getEmployee(name)
    if (employee == null)
      cask.Response(null, statusCode = 404)
    else cask.Response(employee)
  }

  @cask.getJson("/employees")
  def getEmployees(): List[Employee] = {
    logger.info("employees")
    repository.getEmployees()
  }

  //TODO 35 => use pattern matching to cope with Try[List[Employee]]
  @cask.getJson("/employees-by-name")
  def getSomeEmployees(name: Seq[String]): List[Employee] = {
    logger.info(s"employees: $name")
    repository.getEmployees(name.toList)
  }

  //Application plumbing, you may ignore this for now.
  protected[api] def run(): Unit = initialize()
}

object Routes {
  //Application plumbing, you may ignore this for now.
  def apply(repository: Repository): Routes = {
    val routes = new Routes(repository)
    routes.run()
    routes
  }
}
