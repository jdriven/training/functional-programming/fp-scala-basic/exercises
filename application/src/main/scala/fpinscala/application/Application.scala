package fpinscala.application

import com.typesafe.scalalogging.LazyLogging
import fpinscala.application.api.Routes
import fpinscala.application.repository.Repository

object Application extends LazyLogging {

  //TODO 4 => remove the parentheses from this method
  //TODO 5 => remove the return type from this method
  private def start(): Unit = {
    logger.info("Application starting")
    //TODO 9 => call Repository with its syntactic sugar/factory method => Repository()
    val repository = new Repository(Repository.con)
    val routes     = Routes(repository)
    routes.main(Array())
    logger.info("Application started")
  }

  //TODO 1 => run the tests - they should fail
  //TODO 2 => make the application able to start
  //TODO 3:
  // (make sure the Application is NOT running in the background if you started it in the previous step)
  // => run the tests - they should succeed from now on.
  def main(args: Array[String]): Unit = start
}
