package fpinscala.application

import fpinscala.application.domain.Employee
import upickle.default._
import zio._
import zio.test.Assertion._
import zio.test._

import scala.language.postfixOps

object ApplicationTest extends DefaultRunnableSpec {
  val host = "http://localhost:8080"

  override def spec: ZSpec[Environment, Failure] =
    suite("ApplicationSpec")(
      testM("return employee by first name if it exists") {
        for {
          employee <- UIO(requests.get(s"$host/employee/captain", check = false))
        } yield
          assert(employee.statusCode)(equalTo(200)) &&
            assert(employee.text())(equalTo(write(Employee("Captain", "Jack"))))
      },
      testM("return employee by last name if it exists") {
        for {
          employee <- UIO(requests.get(s"$host/employee/unlimited", check = false))
        } yield
          assert(employee.statusCode)(equalTo(200)) &&
            assert(employee.text())(equalTo(write(Employee("2", "Unlimited"))))
      },
      testM("return 404 if employee doesnt exist") {
        for {
          employee <- UIO(requests.get(s"$host/employee/eminem", check = false))
        } yield assert(employee.statusCode)(equalTo(404))
      },
      testM("returns all employees") {
        for {
          employees <- UIO(requests.get(s"$host/employees", check = false))
          employees <- Task(read[List[Employee]](employees).filter(_ != null))
        } yield assert(employees.size)(isGreaterThan(2))
      },
      testM("returns some employees") {
        for {
          employees <- UIO(requests.get(s"$host/employees-by-name?name=eminem&name=ricky", check = false))
          employees <- Task(read[List[Employee]](employees).filter(_ != null))
        } yield assert(employees)(equalTo(List(Employee("Ricky", "Martin"))))
      }
    ).provideCustomLayerShared(application)

  private val checkUntilAlive: UIO[Unit] = Task(requests.get(host))
    .filterOrFail(_.statusCode == 200)(new RuntimeException("Unresponsive"))
    .retryN(200)
    .tapBoth(e => UIO(println(e)), r => UIO(println(r.text())))
    .ignore

  private val application =
    ZLayer.fromAcquireRelease(for {
      f <- Task(Application.main(Array())).fork.interruptible
      _ <- checkUntilAlive
    } yield f)(fiber => {
      //fiber.join or Fiber.interrupt will not work here, hangs the test
      fiber.interrupt.map(
        ex => println(s"stopped with exitCode: $ex")
      )
    })
}
