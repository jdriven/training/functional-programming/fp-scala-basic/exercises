val commonSettings = Seq(
  scalaVersion := "2.13.5",
  addCompilerPlugin(("org.typelevel" %% "kind-projector" % "0.11.3").cross(CrossVersion.full)),
  scalacOptions ++= Seq("-deprecation", "-feature", "-language:implicitConversions")
)

lazy val root = (project in file("."))
  .aggregate(exercises, answers)
  .settings(commonSettings)
  .settings(name := "fpinscala")

lazy val exercises = (project in file("exercises"))
  .settings(commonSettings)
  .settings(name := "exercises")
  .settings(
    libraryDependencies ++= Seq(
      "org.scalatest"     %% "scalatest"                % "3.2.7"    % Test,
      "org.scalacheck"    %% "scalacheck"               % "1.15.3"   % Test,
      "org.scalatestplus" %% "scalacheck-1-15"          % "3.2.7.0"  % Test,
      "org.scalatestplus" %% "scalatestplus-scalacheck" % "1.0.0-M2" % Test
    )
  )

lazy val answers = (project in file("answers"))
  .settings(commonSettings)
  .settings(name := "answers")

lazy val zioVersion = "1.0.7"
lazy val applicationTestDependencies = Seq(
  "com.lihaoyi" %% "requests"          % "0.6.8"    % Test,
  "dev.zio"     %% "zio-test"          % zioVersion % "test",
  "dev.zio"     %% "zio-test-sbt"      % zioVersion % "test",
  "dev.zio"     %% "zio-test-magnolia" % zioVersion % "test"
)
lazy val applicationDependencies = Seq(
  "com.lihaoyi"                %% "cask"           % "0.7.10",
  "dev.zio"                    %% "zio"            % zioVersion,
  "com.typesafe.scala-logging" %% "scala-logging"  % "3.9.3",
  "ch.qos.logback"             % "logback-classic" % "1.2.3",
  "com.h2database"             % "h2"              % "1.4.200"
) ++ applicationTestDependencies

lazy val application = (project in file("application"))
  .settings(commonSettings)
  .settings(name := "application")
  .settings(libraryDependencies ++= applicationDependencies)
  .settings(testFrameworks += new TestFramework("zio.test.sbt.ZTestFramework"))
