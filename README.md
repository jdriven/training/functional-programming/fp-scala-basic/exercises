# Functional Programming in Scala

This course is based on the red book [Functional Programming in Scala](https://www.manning.com/books/functional-programming-in-scala) and it's exercises.
The exercises and answers in this repository for the book are copied and modified from the official [GitHub page](https://github.com/fpinscala/fpinscala).

The course is a four day course (click on the links for the slides):
- [Day 1: Chapter 1, 2, and 3](https://jdriven.gitlab.io/training/functional-programming/fp-scala-basic/fp-in-scala-slides-day-1)
- [Day 2: Chapter 4 and 5](https://jdriven.gitlab.io/training/functional-programming/fp-scala-basic/fp-in-scala-slides-day-2)
- [Day 3: Chapter 10 and 11](https://jdriven.gitlab.io/training/functional-programming/fp-scala-basic/fp-in-scala-slides-day-3)
- [Day 4: Chapter 13](https://jdriven.gitlab.io/training/functional-programming/fp-scala-basic/fp-in-scala-slides-day-4)

Although the other chapters of the book are also very interesting, the chapters we chose for this course are in our opinion the 'basics' of Functional Programming. 
The other chapters contain more advanced topics which are most likely not needed in your day to day work.

# Prerequisits

The following tools are required:
- Java 15 (note: older versions may work aswell)
- [SBT](https://www.scala-sbt.org/index.html)
- git

It is recommended to use IntelliJ as your IDE with the `Scala` plugin by JetBrains installed.

## Quick start when sbt is installed:

  * compile and run tests
        sbt test
  * start sbt shell
        sbt
  * then:
        ;clean;compile;test:compile;test    
  * from IntelliJ with the scala plugin installed:
        right click exercises/scr/test/scala --> run 'Scalatests' in scala
    
# Before this course

Make sure you have the book, either a physical version or a digital one.
And it is recommended to read the first three chapters before starting this course, and the exercises can be skipped for now.
It is not required to understand everything when you read it for the first time, the first day of this course will touch on all the important subjects of these chapters.

# Day 1 exercises
The exercises for all days can be found in the `exercises` subproject.
For day 1 we will do all the exercises in the packages `chapter2` and `chapter3`.

In the subproject `application` is a small application which we will work on and refactor during all the days of the course.
The application will resemble a 'real-world' usage of all the things we learn from this course, as the exercises from the book are abstract and a bit theoretical.

# Day 2 exercises
# Day 3 exercises
# Day 4 exercises
